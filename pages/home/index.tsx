import React from "react";
import MainLayout from "../../layouts/MainLayout";
import "../../public/scss/style.scss";
import { Container, Row, Col } from "react-bootstrap";
import Link from "next/link";

export default class HomeScreen extends React.Component {
  render() {
    return (
      <MainLayout title="XO">
        <Container fluid>
          <Row>
            <Col xs="12" lg="9" className="content">
              <Row className="justify-content-center">
                <Col
                  xs="12"
                  lg="4"
                  className="text-center justify-content-center elem"
                >
                  <img className="logo" src="/images/logo.png" />
                  <div className="stats">
                    <p>سلام امیرحسین!</p>
                    <p>۱۲ برد ۲۸ باخت</p>
                    <p>عضو از ۵ مهر ۱۳۹۸</p>
                  </div>
                  <Link href="/search">
                    <h1 className="button mt-5">شروع بازی</h1>
                  </Link>
                </Col>
              </Row>
            </Col>
            <Col xs="12" md="3" className="onlinesSide">
              <h2 className="text-center">
                بازیکنان آنلاین(<span id="onlineCount">۸</span> نفر)
              </h2>
              <ul className="onlines">
                <li>
                  <span>100.</span> علی شکوری راد
                </li>
              </ul>
              {/* <h1 className="button footerStart">شروع بازی</h1> */}
            </Col>
          </Row>
        </Container>
      </MainLayout>
    );
  }
}
