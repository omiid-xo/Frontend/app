import React from "react";
import { NextPageContext } from "next";
import Router from "next/router";

export default class Index extends React.Component {
  static async getInitialProps({ res }: NextPageContext) {
    const seen = false;
    if (res && !seen) {
      res.writeHead(302, {
        Location: "/splash"
      });
      res.end();
    } else {
      Router.push("/login");
    }
  }
}
