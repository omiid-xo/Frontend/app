import React from "react";
import MainLayout from "../../layouts/MainLayout";
import { Container, Row, Col } from "react-bootstrap";
import "../../public/scss/style.scss";

export default class SearchScreen extends React.Component {
  render() {
    return (
      <MainLayout title="Waiting for opponent">
        <Container fluid>
          <Row className="justify-content-center">
            <Col
              xs="12"
              lg="4"
              className="searching justify-content-center text-center"
            >
              <h1>در حال پیداکردن حریف</h1>
              <img src="/images/battle.svg" className="battle" />
              <p className="light">طوری نبرید که مردم فکر کنند طوری شده!</p>
            </Col>
          </Row>
        </Container>
      </MainLayout>
    );
  }
}
