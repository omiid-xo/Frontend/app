import React from "react";
import MainLayout from "../../layouts/MainLayout";
import "../../public/scss/style.scss";
import {
  Formik,
  Form,
  Field,
  FormikProps,
  FormikValues,
  FormikActions
} from "formik";
import * as Yup from "yup";
import { Container, Row, Col } from "react-bootstrap";
import Router from "next/router";

const LoginSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  password: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});

export default class LoginScreen extends React.Component {
  state: {
    error: string;
  } = {
    error: ""
  };

  isDisabled(props: FormikProps<any>): boolean {
    if (props.isSubmitting) {
      return true;
    } else {
      return !props.isValid;
    }
  }

  onSubmit(values: FormikValues, action: FormikActions<any>) {
    // const self = this;
    setTimeout(() => {
      // self.setState({ error: "رمز عبور وارد شده اشتباه است" }, () => {
      //   action.setSubmitting(false);
      //   action.resetForm({ username: values["username"], password: "" });
      // });
      Router.push("/home");
    }, 2000);
  }

  render() {
    return (
      <MainLayout title="Login">
        <Container fluid>
          <Row className="justify-content-md-center">
            <Col xs="12" lg="3" className="splashContainer text-center">
              <img className="logo" src="/images/logo.png" />
              <h1>بازی دوز</h1>
              <p className="light">
                اگه قبلا لاگین کردین از نام‌کاربری و رمزعبور خودتون وگرنه از یه
                نام کار‌بری و رمزعبور دلخواه استفاده کنین
              </p>
              <Formik
                initialValues={{ username: "", password: "" }}
                validationSchema={LoginSchema}
                onSubmit={this.onSubmit.bind(this)}
              >
                {props => (
                  <Form>
                    <Field type="text" name="username" />
                    <Field type="password" name="password" />
                    <p className="error">{this.state.error}</p>
                    <button
                      className="button"
                      type="submit"
                      disabled={this.isDisabled(props)}
                    >
                      ورود
                    </button>
                  </Form>
                )}
              </Formik>
            </Col>
          </Row>
        </Container>
      </MainLayout>
    );
  }
}
