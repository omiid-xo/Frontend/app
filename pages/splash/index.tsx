import React from "react";
import Link from "next/link";
import "../../public/scss/style.scss";
import { Container, Row, Col } from "react-bootstrap";

import MainLayout from "../../layouts/MainLayout";

export default class Splash extends React.Component {
  render() {
    return (
      <MainLayout title="Omiid XO">
        <Container fluid>
          <Row className="justify-content-md-center">
            <Col xs="12" lg="3" className="splashContainer text-center">
              <img className="logo" src="/images/logo.png" />
              <h1>بازی دوز</h1>
              <p>
                دوز یه بازی متعلق به مصر باستان هستش که تقریبا همه‌مون تو بچگی
                بازی کردیم و کلی خاطره داریم باهاش!‌
              </p>
              <Link href="/login">
                <div className="button mt-5">ورود به بازی</div>
              </Link>
            </Col>
          </Row>
        </Container>
      </MainLayout>
    );
  }
}
