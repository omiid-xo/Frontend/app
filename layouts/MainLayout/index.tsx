import React, { ReactNode } from "react";
import Head from "next/head";

interface IProps {
  children: ReactNode;
  title: string;
}

export default class MainLayout extends React.Component<IProps> {
  render() {
    const { children, title } = this.props;
    return (
      <div>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <title>{title}</title>
          <link rel="stylesheet" href="/css/bootstrap.min.css" />
          <link rel="stylesheet" href="/css/fontiran.css" />
          <link rel="stylesheet" href="/css/lalehzar.css" />
          {/* <link rel="icon" href="/favicon.ico" /> */}
        </Head>
        {children}
      </div>
    );
  }
}
